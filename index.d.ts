import Vue, {ComponentOptions, PluginFunction} from "vue";

export type CustomEventBusInstance = Vue;
export type CustomEventBusGenerator = (rootComponent: Vue) => Vue;
export type CustomEventBus = CustomEventBusInstance | CustomEventBusGenerator;

export interface HitchyWidgetsEventBus {
    $emit(name: string, ...args: any): void;

    $on(name: string, callback: (...args: any) => void): void;

    $once(name: string, callback: (...args: any) => void): void;

    $off(name: string, callback: (...args: any) => void): void;
}

export type HitchyVueWidgetsRouteFactory = () => Array<object>;

declare module "vue/types/options" {
    interface ComponentOptions<V extends Vue> {
        /**
         * Selects custom prefix to use on globally exposing widgets. Default
         * prefix is `hitchy`, thus exposing e.g. `hitchy-card` component.
         */
        prefix?: string;

        /**
         * Provides custom event bus or factory for event bus used per component
         * tree. If omitted, every component tree is using its own event bus
         * automatically.
         */
        eventBus?: CustomEventBus;

        /**
         * Selects locale to use explicitly in context of application.
         */
        locale?: string;
    }
}

declare module "vue/types/vue" {
    interface Vue {
        /**
         * Exposes event bus of current component tree used by @hitchy/vue-widgets
         * for cross-component communication.
         */
        $hitchyEventBus: HitchyWidgetsEventBus;
    }
}

declare class HitchyVueWidgets {
    static install: PluginFunction<never>;
}

export default HitchyVueWidgets;

/**
 * Combines exposure of descriptions for components implemented in plugin.
 */
export interface components {
    List: object;
    ModelList: object;
    Card: object;
    ModelCard: object;

    ModalPopup: object;
}
