# @hitchy/vue-widgets

_common [Vue.js](https://vuejs.org) components for use with [Hitchy](https://hitchy.org)-based applications_

## License

MIT

## Usage

Install this package:

```bash
npm i @hitchy/vue-widgets
```

In your application's **main.js** pass this package to **Vue.use()**:

```javascript
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import HitchyVueWidgets from "@hitchy/vue-widgets";

Vue.config.productionTip = false;

Vue.use( HitchyVueWidgets );

new Vue( {
    router,
    store,
    render: h => h( App )
} ).$mount( "#app" );
```

This will register components of this package with prefix `hitchy-` as global components of your application. Using options on calling **Vue.use()** you can pick a different prefix to use:

```javascript
Vue.use( HitchyVueWidgets, { prefix: "my-custom" } );
```

In this case, component `hitchy-list` will be exposed as `my-custom-list` instead.

### Setting Up For Hitchy Model Browsing

If you intend to use the additional support for browsing Hitchy models, you need to use a slightly different **main.js** file:

```javascript
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import HitchyVueWidgets from "@hitchy/vue-widgets";
import "@hitchy/vue-widgets/theme/default.scss";

Vue.config.productionTip = false;

const eventBus = new Vue();

Vue.use( HitchyVueWidgets, { eventBus } );

new Vue( {
    router: router( eventBus ),
    store,
    render: h => h( App )
} ).$mount( "#app" );
```

In this case router configuration in **router.js** is provided using generator function to gain access on event bus created in **main.js** before. The according implementation in **router.js** should be similar to this one:

```javascript
import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Models from "../views/Models.vue";
import NotFound from "../views/NotFound.vue";
import { Model } from "@hitchy/plugin-odem-rest-client";
import { hitchyCreateModelRoutes } from "@hitchy/vue-widgets";

Vue.use( VueRouter );

/**
 * Generates instance of vue router.
 *
 * @param {Vue} eventBus instance of event bus to use for application-wide notifications
 * @returns {VueRouter} generated instance of vue router
 */
export default function( eventBus ) {
    const routes = [
        {
            path: "/",
            redirect: "/home",
        },
        {
            path: "/home",
            component: Home,
        },
        {
            path: "/list",
            component: Models,
            children: hitchyCreateModelRoutes( eventBus ),
        },
        {
            path: "*",
            component: NotFound,
        },
    ];

    const router = new VueRouter( {
        mode: "history",
        base: process.env.BASE_URL,
        routes
    } );

    router.beforeEach( ( to, from, next ) => {
        Model.getModels()
            .then( () => {
                next();
            } )
            .catch( error => {
                next( { name: "error", error } );
            } );
    } );

    return router;
}
```

This configuration is

* integrating vue widgets for browsing all models provided by Hitchy using path prefix `/list`,
* assuring schemata of models being fetched from Hitchy backend as early as possible and
* setting up some custom views of your application as examples.

### Styling Components

Basically, this plugin doesn't come with a particular design applied to provided components, by default. You might want to apply a provided design instead of your own explicitly, though.

In your application's **main.js** file import the design definition you like to use by inserting

```javascript
import "@hitchy/vue-widgets/theme/default.scss";
```
