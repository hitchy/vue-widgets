/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

import List from "./components/List.vue";
import ModelList from "./components/ModelList.vue";

import Card from "./components/Card.vue";
import ModelCard from "./components/ModelCard.vue";

import ModalPopup from "./components/ModalPopup.vue";

import ListView from "./views/ListView";
import CardView from "./views/CardView";
import NotFound from "./views/NotFound";

import { Model } from "@hitchy/plugin-odem-rest-client";
import i18n, { normalizeLocale } from "@cepharum/vue-i18n";


/**
 * Implements installation code for plugin according to Vue plugin ecosystem for
 * use with Vue.use().
 *
 * @param {class<Vue>} Vue class of Vue this plugin is used with
 * @param {string} prefix prefix of custom components exposed in scope of provided Vue
 * @param {Vue.Component} eventBus custom event bus to use for all running Vue applications
 * @param {string} locale locale to use explicitly instead of browser's current locale
 * @returns {void}
 */
export function install( Vue, { prefix = "hitchy", eventBus, locale = null } = {} ) {
	Vue.use( i18n );

	Vue.component( `${prefix}-list`, List );
	Vue.component( `${prefix}-model-list`, ModelList );

	Vue.component( `${prefix}-card`, Card );
	Vue.component( `${prefix}-model-card`, ModelCard );

	Vue.component( `${prefix}-modal-popup`, ModalPopup );

	Vue.mixin( {
		created() {
			if ( this.$root === this ) {
				// at root of current Vue application -> expose event bus
				this.$hitchyEventBus = typeof eventBus === "function" ? eventBus( this ) : eventBus;

				if ( !this.$hitchyEventBus ) {
					this.$hitchyEventBus = new Vue( {
						parent: this,
					} );
				}

				if ( this.$i18n ) {
					this.$i18nLoad( "en", require( "./l10n/en" ).default );

					const normalized = normalizeLocale( locale || navigator.language );
					if ( normalized ) {
						switch ( normalized.replace( /-.+$/, "" ) ) {
							case "de" :
								this.$i18nLoad( "de", require( "./l10n/de" ).default );
								break;
						}
					}
				}
			} else {
				this.$hitchyEventBus = this.$root.$hitchyEventBus;
			}
		}
	} );

	Vue.hitchyCreateModelRoutes = hitchyCreateModelRoutes;
}

/**
 * Generates configuration suitable for injecting a whole set of views into
 * vue-router for managing Hitchy-based models.
 *
 * @param {Vue} eventBus instance of event bus to use for controlling modal popups
 * @returns {Array<object>} lists route configurations for injecting as children to some parent route
 */
export function hitchyCreateModelRoutes( eventBus ) {
	return [
		{
			path: ":slug",
			component: ListView,
			props: true,
		},
		{
			path: ":slug/add",
			component: CardView,
			props: true,
			beforeEnter( to, from, next ) {
				to.params.edit = true;
				next();
			},
		},
		{
			path: ":slug/:uuid/edit",
			component: CardView,
			props: true,
			beforeEnter( to, from, next ) {
				to.params.edit = true;
				next();
			},
		},
		{
			path: ":slug/:uuid/delete",
			beforeEnter( to, from, next ) {
				if ( eventBus ) {
					const alert = {
						title: "@@POPUP_REMOVE_TITLE",
						text: "@@POPUP_REMOVE_TEXT",
						buttons: [ "@@BUTTON_NO", "@@BUTTON_YES" ],
					};

					eventBus.$emit( "show-modal", alert, clicked => {
						if ( clicked === 1 ) {
							const SelectedModel = Model.tryModel( Model.slugToName( to.params.slug ) );
							const item = new SelectedModel( to.params.uuid );

							item.remove()
								.catch( error => {
									this.$hitchyEventBus.$emit( "show-modal", {
										title: "@@POPUP_ERROR_TITLE",
										text: "@@POPUP_REMOVE_ERROR",
										details: error.message
									} );
								} );

							next( from.name || from.fullPath );
						} else {
							next( false );
						}
					} );
				}
			},
		},
		{
			path: ":slug/:uuid",
			component: CardView,
			props: true,
		},
		{
			path: "*",
			alias: "no-such-model",
			component: NotFound,
			props: true,
		},
	];
}

export const components = { List, ModelList, Card, ModelCard, ModalPopup };

export default { install };
